/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pattern.builder;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class MealBuilder
 */
public interface MealBuilder {
 
     public void buildDrink(); 
    
     public void buildMainCourse();
     
     public void  buildSide();
     
     public Meal getMeal();
}