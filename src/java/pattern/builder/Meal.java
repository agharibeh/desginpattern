/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pattern.builder;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class Meal
 */
public class Meal {

    private String drink;
    private String mainCourse;
    private String side;

    /**
     *
     * @return
     */
    public String getDrink() {
        return drink;
    }

    /**
     *
     * @param drink
     */
    public void setDrink(String drink) {

        this.drink = drink;
    }

    /**
     *
     * @return
     */
    public String getMainCourse() {
        return mainCourse;
    }

    /**
     *
     * @param mainCourse
     */
    public void setMainCourse(String mainCourse) {
        this.mainCourse = mainCourse;
    }

    /**
     *
     * @return
     */
    public String getSide() {
        return side;
    }

    /**
     *
     * @param side
     */
    public void setSide(String side) {
        this.side = side;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "drink : " + drink + ", main course : " + mainCourse + ", side: " + side;
    }
}
