/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pattern.AbstractFactory.animals;

/**
 * 
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class Tyrannosaurus 
 * @extands Animal
 */

public class Tyrannosaurus extends Animal {

    @Override
    public String makeSound() {
        return "Roar";
    }
}