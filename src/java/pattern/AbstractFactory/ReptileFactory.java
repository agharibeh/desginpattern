/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pattern.AbstractFactory;

import pattern.AbstractFactory.animals.Animal;
import pattern.AbstractFactory.animals.Snake;
import pattern.AbstractFactory.animals.Tyrannosaurus;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class Abstract Factory 
 */

public class ReptileFactory extends  SpeciesFactory{
    @Override
    public Animal getAnimal(String type){
     if("snake".equals(type)){
         return new Snake();
     }else{
         return new Tyrannosaurus();
     }
    }
}
