/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pattern.AbstractFactory;

import pattern.AbstractFactory.animals.Animal;
/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class Species Factory  
 */
public  abstract  class SpeciesFactory {
    public abstract Animal getAnimal(String type);
}
