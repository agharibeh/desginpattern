/**
 * The abstract factory pattern is a creational design pattern. 
 * An abstract factory is a factory that returns factories. Why is this layer of abstraction useful? A normal factory can be used to create      sets of related objects.
 * An abstract factory returns factories. Thus, an abstract  factory is used to return factories that can be used to create sets of related      objects. 
 */

package pattern.AbstractFactory;
/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class Abstract Factory 
 */

public class AbstractFactory {
    
    public SpeciesFactory getSpeciesFactory(String type){
        if("mammal".equals(type)){
            return new MammalFactory();
        }else {
            return new ReptileFactory();
        }
    }   
}