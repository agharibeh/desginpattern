/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pattern.singleton;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class EagerInitializedSingleton
 */
public class StaticBlockSingleton {
    private static  StaticBlockSingleton instance; 
    private StaticBlockSingleton() {};
    
    static {
     try {
        instance =  new StaticBlockSingleton();
     }catch (Exception e ){
       throw new RuntimeException("Exeption Occured in creating Singleton Instance");
     }
    }
    
    public  static  StaticBlockSingleton getInstance(){
       return instance;
    }
}
