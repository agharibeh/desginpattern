/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pattern.singleton;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @enum EnumSingleton
 */
public enum  EnumSingleton {
    INSTANCE;   
    
    public static void  doSomething(){
      // do something
    }
}