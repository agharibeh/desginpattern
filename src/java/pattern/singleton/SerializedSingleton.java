/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pattern.singleton;

import java.io.Serializable;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class EnumSingleton
 */
public class SerializedSingleton implements Serializable {

    private static final long serialVersionUID = 1;

    private SerializedSingleton() {  }
    /**
     * 
     */
    private static class SingletonHelper{
      private static final SerializedSingleton instance = new SerializedSingleton();
    }
    /**
     * 
     * @return 
     */
    public static SerializedSingleton getIntance (){
      return SingletonHelper.instance;         
    }
}