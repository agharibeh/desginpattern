/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pattern.singleton;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;


/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @enum SingletonSerializedTest
 */
public class SingletonSerializedTest {
     public static void  main (String[] args ) throws FileNotFoundException ,IOException , ClassNotFoundException {
         SerializedSingleton instanceOne = SerializedSingleton.getIntance();
         ObjectOutput out = new ObjectOutputStream( new FileOutputStream("filename.ser"));
         out.writeObject(instanceOne);
         out.close();
         
         ObjectInput in  = new ObjectInputStream(new FileInputStream("filename.ser"));
         SerializedSingleton instanceTwo;
         instanceTwo = (SerializedSingleton) in.readObject();
         in.close();
         
         System.out.println("InstanceOne HashCode = "+ instanceOne.hashCode());
         System.out.println("InstanceOne HashCode = "+ instanceTwo.hashCode());         
     } 
}