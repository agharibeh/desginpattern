/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pattern.singleton;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class BillPughSingleton
 */
public class BillPughSingleton {
    
    private BillPughSingleton(){}

    private static class SingletonHelper {
      private  static  final BillPughSingleton INSTANCE = new BillPughSingleton();
    }
    
    public static BillPughSingleton getInstance(){
     return SingletonHelper.INSTANCE;
    }
}
