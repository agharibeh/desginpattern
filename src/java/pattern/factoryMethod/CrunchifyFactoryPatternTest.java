/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pattern.factoryMethod;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class MealBuilder
 */
public class CrunchifyFactoryPatternTest {
     
    public static void main(String[] agrs){
         
        CrunchifiyCompany ebay = CrunchfiyFactoryPattern.getDetails("ebay", "480.123.4567", "98765");
        CrunchifiyCompany google = CrunchfiyFactoryPattern.getDetails("google", "519.123.4567", "56789");
        
        System.out.println("Factory ebay Config :" + ebay);
        System.out.println("Factory Google Config :"+ google);
    }
}