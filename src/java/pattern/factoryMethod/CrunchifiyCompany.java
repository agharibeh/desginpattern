package pattern.factoryMethod;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class CrunchifiyCompany
 */ 
public  abstract class CrunchifiyCompany {
    
    public abstract String  getPhoneNumber();
    
    public abstract String getZipCode();
    
    @Override
    public String  toString (){
        
       return "Phone #= " + this.getPhoneNumber() +", Zip Code= " +this.getZipCode();
       
    }
}