/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pattern.factoryMethod;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class CrunchifyGoogle
 */
public class CrunchifyGoogle extends CrunchifiyCompany{
    
    private  String phoneNumber;
    private  String zipCode;
    
    
    public CrunchifyGoogle(String phoneNumber , String zipCode)
    {
        this.phoneNumber = phoneNumber;
        this.zipCode = zipCode;
    
    }
    
    @Override 
    public String getPhoneNumber() {
        return this.phoneNumber;   
    }
    
    @Override 
    public String getZipCode(){
        return this.zipCode;
    }
}
