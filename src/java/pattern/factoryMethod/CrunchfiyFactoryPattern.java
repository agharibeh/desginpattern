/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pattern.factoryMethod;

/**
 * @author <a herf="mailto:amg2255@gmail.com" >Ahmad Gharibeh</a>
 * @version 0.5
 * @class CrunchifyFactoryPattern
 */
public class CrunchfiyFactoryPattern {

    public static CrunchifiyCompany getDetails(String type, String phoneNumber, String zipCode) {
        if ("Ebay".equalsIgnoreCase(type)) {
            return new CrunchifyEbay(phoneNumber, zipCode);
        } else if ("Google".equalsIgnoreCase(type)) {
            return new CrunchifyGoogle(phoneNumber, zipCode);
        }
        return null;
    }
}